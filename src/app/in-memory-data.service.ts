import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Iron Man' },
      { id: 2, name: 'Spiderman' },
      { id: 3, name: 'Thor' },
      { id: 4, name: 'Captain America' },
      { id: 5, name: 'Hulk' },
      { id: 6, name: 'Captain Marvel' },
      { id: 7, name: 'Black Panther' },
      { id: 8, name: 'Antman' }
    ];
    return {heroes};
}

genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
